import telebot
import httplib2
import os
import time
import base64
import threading
import email
import json
from functools import partial
import io
from bs4 import BeautifulSoup

from apiclient import discovery, errors
import oauth2client
from oauth2client import client, tools


class Identity():

    def __init__(self, bot, settings, config, name):
        self.settings = settings
        self.config = config
        self.name = name
        self.bot = bot
        self.credentials = self.get_credentials()
        self.http = self.credentials.authorize(httplib2.Http())
        self.service = discovery.build('gmail', 'v1', http=self.http)

    def get_credentials(self):
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'gmail-telegram-bot-%s.json' % self.name)
        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.config['client_secret'], self.settings['scopes'])
            flow.user_agent = self.settings['application_name']
            credentials = tools.run_flow(flow, store, None)  # flags = None
            print('Storing credentials to ' + credential_path)
        return credentials

    def query(self):
        filter_str = ' '.join(self.config['filters'])
        addresses = ['from:'+addr for addr in self.config['addresses']]

    def query(self):
        filter_str = ' '.join(self.config['filters'])
        addresses =  ['from:'+addr for addr in self.config['addresses']]
        if len(addresses) == 1:
            return addresses[0] + filter_str
        elif len(addresses) < 1:
            return filter_str
        return '(' + ' OR '.join(addresses) + ')' + ' AND ' + filter_str

    def gmail_polling(self):
        print('polling ' + self.name)
        t = threading.Timer(5.0, self.gmail_polling)
        t.daemon = True  # to cleanly shutdown the program
        t.start()
        messages = self.list_messages()
        for msg in messages:
            self.mark_message_as_read(msg['id'])
            for target in self.config['send_messages_to']:
                sent_subject = False
                subject, parts = self.get_message_body(msg['id'])
                for msg_type, msg_body in parts:
                    if target in self.config['send_subject_to'] and not sent_subject:
                        self.bot.send_message(target, subject)
                        sent_subject = True
                    if msg_body:
                        if msg_type == 'text' and msg_body.strip():
                            self.bot.send_message(target, msg_body)
                        if msg_type == 'image' and msg_body:
                            thing = io.BytesIO(msg_body)
                            thing.name = 'pic.jpg'
                            self.bot.send_photo(target, thing)

    def get_message_body(self, msg_id):
        try:
            message = self.service.users().messages().get(userId='me', id=msg_id, format='raw').execute()
            msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
            mime_msg = email.message_from_bytes(msg_str)
           
            parts = []
            found_plaintext = False

            for part in mime_msg.walk():
                maintype = part.get_content_maintype()
                subtype = part.get_content_subtype()
                subject = mime_msg['subject']

                if maintype == 'multipart':
                    continue
                elif maintype == 'text' and subtype == 'plain':
                    if not found_plaintext:
                        parts.append(('text', part.get_payload(decode=True)))
                        found_plaintext = True
                elif maintype == 'text' and subtype == 'html':
                    if not found_plaintext:
                        soup = BeautifulSoup(part.get_payload(decode=True), 'html.parser')
                        [s.extract() for s in soup.findAll(['script', 'style'])]
                        parts.append(('text', soup.get_text()))
                        found_plaintext = True
                elif maintype == 'image':
                    img_data = base64.urlsafe_b64decode(part.get_payload())
                    parts.append(('image', img_data))
            print(parts)
            return (subject, parts)
        except errors.HttpError as error:
            print('An error occurred: %s' % error)


    def mark_message_as_read(self, msg_id):
        try:
            label_modify_body = {'removeLabelIds': ['UNREAD'], 'addLabelIds': []}
            message = self.service.users().messages().modify(userId='me', id=msg_id, body=
                label_modify_body).execute()
            print(message)
            return message
        except errors.HttpError as error:
            print('An error occurred: %s' % error)

    def list_messages(self):
        try:
            query = self.query()
            response = self.service.users().messages().list(userId='me', q=query).execute()

            messages = []
            if 'messages' in response:
                messages.extend(response['messages'])

            while 'nextPageToken' in response:
                page_token = response['nextPageToken']
                response = self.service.users().messages().list(userId='me', q=query,
                                                 pageToken=page_token).execute()
                messages.extend(response['messages'])

            return messages
        except errors.HttpError as error:
            print('An error occurred: %s' % error)

json_config = None
KB_HIDE = telebot.types.ReplyKeyboardHide()
selected_ident = None
marked_targets = None
selected_targets = None
chat_id_cache = {}

def main():
    json_config = None
    with open('config.json') as json_config:
        json_config = json.load(json_config)

    settings = json_config['settings']

    bot = telebot.TeleBot(settings['telegram_api_token'])
    identities = []
    for name in json_config['identities']:
        identities.append(Identity(bot, settings, json_config['identities'][name], name))

    def save_config():
        with open('config.json', 'w') as outfile:
            json.dump(json_config, outfile, indent=2)

    def is_admin(id):
        return id in settings['admins']

    @bot.message_handler(commands=['start'])
    def send_start(message):
        if is_admin(message.from_user.id):
            bot.reply_to(message, settings['welcome_msg'])

    @bot.message_handler(commands=['help'])
    def send_welcome(message):
        if is_admin(message.from_user.id):
            bot.reply_to(message, """
/addadmin - add administrator
/remadmin - remove administrator
/subjects - see for which addresses we send subject
/togglesubject - toggles sending the subject
/sendmessage - send messages
/identities - list identities
/targets - list targets for identity
/filters - list filters for identity
/addresses - list addresses for identity
/query - print full filter query for identity
/addfilter - add filter(s) to identity
/remfilter - remove filter(s) from identity
/addaddress - add address(es) to identity
/remaddress - remove address(es) from identity
/addtarget - add target(s) to identity
/remtarget - remove target(s) from identity
""")

    def get_display_name(chat_id):
        if chat_id in chat_id_cache.keys():
            return chat_id_cache[chat_id]
        try:
            chat = bot.get_chat(chat_id)
            if chat.title:
                chat_id_cache[chat_id] = chat.title
                return chat.title
            if chat.username:
                chat_id_cache[chat_id] = chat.username
                return chat.username
        except Exception:
            pass
        return str(chat_id)

    def display_name_to_chat_id(display_name):
        for id, name in chat_id_cache.items():
            if name == display_name:
                return id
        return display_name

    @bot.message_handler(commands=['addadmin'])
    def add_admin(message):
        def process_admin_step(message):
            if is_admin(message.from_user.id):
                try:
                    settings['admins'].append(int(message.text.split()[0]))
                    save_config()
                    bot.reply_to(message, "Done.")
                except Exception:
                    bot.reply_to(message, "Invalid parameters")

        if is_admin(message.from_user.id):
            msg = bot.reply_to(message, "Enter the user ID for the new admin:")
            bot.register_next_step_handler(msg, process_admin_step)

    @bot.message_handler(commands=['remadmin'])
    def rem_admin(message):
        def admins_markup():
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for admin in settings['admins']:
                markup.add(str(admin) + " (" + get_display_name(admin) + ")")
            markup.add('/done')
            return markup

        def process_admin_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                try:
                    if params[0].startswith('/done'):
                        bot.reply_to(message, "Done.", reply_markup=KB_HIDE)
                        return
                    settings['admins'].remove(int(message.text.split()[0]))
                    save_config()
                    msg = bot.reply_to(message, "Removed. Select more or '/done'.", reply_markup=admins_markup())
                except Exception:
                    msg = bot.reply_to(message, "Invalid parameters", reply_markup=admins_markup())
                bot.register_next_step_handler(msg, process_admin_step)

        if is_admin(message.from_user.id):
            msg = bot.reply_to(message, "Which admin do you want to remove?", reply_markup=admins_markup())
            bot.register_next_step_handler(msg, process_admin_step)

    @bot.message_handler(commands=['subjects'])
    def subjects(message):
        def process_identity_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                out = "Addresses | Subject enabled"
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            for address in ident.config['addresses']:
                                out += "\n%s | " % address
                                if address in ident.config['send_subject_to']:
                                    out += "yes"
                                else:
                                    out += "no"
                            bot.reply_to(message, out, reply_markup=KB_HIDE)
                            return
                    bot.reply_to(message, "Identity %s not found" % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['togglesubject'])
    def toggle_subject(message):
        def addresses_markup():
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for address in selected_ident.config['addresses']:
                markup.add(address)
            return markup

        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please select the address for which you want to toggle sending subject:",
                                reply_markup=addresses_markup())
                            bot.register_next_step_handler(msg, process_addresses_step)
                            return
                    bot.reply_to(message, "Identity %s not found" % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_addresses_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    if params[0] in selected_ident.config['send_subject_to']:
                        selected_ident.config['send_subject_to'].remove(params[0])
                    else:
                        selected_ident.config['send_subject_to'].append(params[0])
                    save_config()
                    bot.reply_to(message, "Done.", reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity do you want to toggle subject?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['sendmessage'])
    def send_message(message):
        def targets_markup():
            global selected_targets
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                for target in ident.config['send_messages_to']:
                    if target not in marked_targets:
                        marked_targets.append(target)
                        markup.add(str(target) + ' (' + get_display_name(target) + ')')
            markup.add('/done')
            return markup

        def process_target_step(message):
            global selected_targets
            if is_admin(message.from_user.id):
                params = message.text.split()
                if params[0].startswith('/done'):
                    msg = bot.reply_to(message, "Enter the message to be sent:", reply_markup=telebot.types.ForceReply())
                    bot.register_next_step_handler(msg, process_message_step)
                else:
                    if params[0] in selected_targets:
                        msg = bot.reply_to(message, "Target already selected - skipping.", reply_markup=targets_markup())
                        bot.register_next_step_handler(msg, process_target_step)
                        return
                    else:
                        selected_targets.append(params[0])
                        msg = bot.reply_to(message, "Selected. Use '/done' when you're ready.", reply_markup=targets_markup())
                        bot.register_next_step_handler(msg, process_target_step)
                        return
                    msg = bot.reply_to(message, "Invalid target - skipping.", reply_markup=targets_markup())
                    bot.register_next_step_handler(msg, process_target_step)


        def process_message_step(message):
            global selected_targets
            if is_admin(message.from_user.id):
                for target in selected_targets:
                    bot.send_message(target, message.text)
                bot.reply_to(message, "Message forwarded.", reply_markup=KB_HIDE)
                selected_targets = []

        if is_admin(message.from_user.id):
            global selected_targets
            global marked_targets
            selected_targets = []
            marked_targets = []
            msg = bot.reply_to(message, "To which target do you want to send the message? Use '/done' when you're ready.", reply_markup=targets_markup())
            bot.register_next_step_handler(msg, process_target_step)

    @bot.message_handler(commands=['identities'])
    def send_idents(message):
        if is_admin(message.from_user.id):
            out = "Identities:"
            for ident in identities:
                out += " " + ident.name
            bot.reply_to(message, out)

    @bot.message_handler(commands=['targets'])
    def send_targets(message):
        def process_identity_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            bot.reply_to(
                                message,
                                '\n'.join(list(map(get_display_name, ident.config['send_messages_to']))),
                                reply_markup=KB_HIDE
                                )
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity do you want to list targets?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['filters'])
    def send_targets(message):
        def process_identity_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            bot.reply_to(
                                message,
                                '\n'.join(ident.config['filters']),
                                reply_markup=KB_HIDE
                                )
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity do you want to list filters?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['addresses'])
    def send_targets(message):
        def process_identity_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            print(ident.config['addresses'])
                            bot.reply_to(
                                message,
                                '\n'.join(ident.config['addresses']),
                                reply_markup=KB_HIDE
                                )
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity do you want to list targets?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['query'])
    def send_targets(message):
        def process_identity_step(message):
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            bot.reply_to(
                                message,
                                ident.query(),
                                reply_markup=KB_HIDE
                                )
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    boy.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "For which identity do you want to print the query?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['addfilter'])
    def add_filter(message):
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please type the filters to add, separated by space:", reply_markup=telebot.types.ForceReply())
                            bot.register_next_step_handler(msg, process_filters_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters", reply_markup=KB_HIDE)

        def process_filters_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                for filter in params:
                    selected_ident.config['filters'].append(filter)
                save_config()
                bot.reply_to(message, "Added %s filters" % str(len(params)), reply_markup=KB_HIDE)
                return

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "To which identity do you want to add filters?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)


    @bot.message_handler(commands=['remfilter'])
    def rem_filter(message):
        def filters_markup():
            global selected_ident
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for filter in selected_ident.config['filters']:
                markup.add(filter)
            markup.add('/done')
            return markup
    
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please select the filters to remove, and select '/done' to stop.", reply_markup=filters_markup())
                            bot.register_next_step_handler(msg, process_filters_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_filters_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    if params[0].startswith('/done'):
                        bot.reply_to(message, "Done.", reply_markup=KB_HIDE)
                        selected_ident = None
                    else:
                        try:
                            selected_ident.config['filters'].remove(params[0])
                            save_config()
                            bot.reply_to(message, "Successfully removed filter. Select another one, or '/done' to stop.", reply_markup=filters_markup())
                        except ValueError:
                            bot.reply_to(message, "Filter %s not found." % params[0], reply_markup=filters_markup())
                        bot.register_next_step_handler(msg, process_filters_step)


        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "From which identity do you want to remove filters?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)

    @bot.message_handler(commands=['addaddress'])
    def add_filter(message):
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please type the addresses to add, separated by space:", reply_markup=telebot.types.ForceReply())
                            bot.register_next_step_handler(msg, process_addresses_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_addresses_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                for address in params:
                    selected_ident.config['addresses'].append(address)
                save_config()
                bot.reply_to(message, "Added %s addresses" % str(len(params)), reply_markup=KB_HIDE)
                return

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "To which identity do you want to add addresses?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)


    @bot.message_handler(commands=['remaddress'])
    def rem_filter(message):
        def addresses_markup():
            global selected_ident
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for address in selected_ident.config['addresses']:
                markup.add(address)
            markup.add('/done')
            return markup
    
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please select the addresses to remove, and select '/done' to stop.", reply_markup=addresses_markup())
                            bot.register_next_step_handler(msg, process_addresses_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_addresses_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    if params[0].startswith('/done'):
                        bot.reply_to(message, "Done.", reply_markup=KB_HIDE)
                        selected_ident = None
                    else:
                        try:
                            selected_ident.config['addresses'].remove(params[0])
                            save_config()
                            bot.reply_to(message, "Successfully removed address. Select another one, or '/done' to stop.", reply_markup=addresses_markup())
                        except ValueError:
                            bot.reply_to(message, "Address %s not found." % params[0], reply_markup=addresses_markup())
                        bot.register_next_step_handler(msg, process_addresses_step)


        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "From which identity do you want to remove addresses?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)


    @bot.message_handler(commands=['addtarget'])
    def add_filter(message):
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please type the target to add, separated by space:", reply_markup=telebot.types.ForceReply())
                            bot.register_next_step_handler(msg, process_addresses_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_addresses_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                for address in params:
                    selected_ident.config['send_messages_to'].append(address)
                save_config()
                bot.reply_to(message, "Added %s targets" % str(len(params)), reply_markup=KB_HIDE)
                return

        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "To which identity do you want to add targets?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)


    @bot.message_handler(commands=['remtarget'])
    def rem_filter(message):
        def targets_markup():
            global selected_ident
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for target in selected_ident.config['send_messages_to']:
                markup.add(get_display_name(target))
            markup.add('/done')
            return markup
    
        def process_identity_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    for ident in identities:
                        if ident.name == params[0]:
                            selected_ident = ident
                            msg = bot.reply_to(message, "Please select the targets to remove, and select '/done' to stop.", reply_markup=targets_markup())
                            bot.register_next_step_handler(msg, process_targets_step)
                            return
                    bot.reply_to(message, "Identity %s not found." % params[0], reply_markup=KB_HIDE)
                else:
                    bot.reply_to(message, "Incorrect parameters")

        def process_targets_step(message):
            global selected_ident
            if is_admin(message.from_user.id):
                params = message.text.split()
                if len(params) == 1:
                    if params[0].startswith('/done'):
                        bot.reply_to(message, "Done.", reply_markup=KB_HIDE)
                        selected_ident = None
                    else:
                        try:
                            selected_ident.config['send_messages_to'].remove(params[0])
                            save_config()
                            bot.reply_to(message, "Successfully removed target. Select another one, or '/done' to stop.", reply_markup=targets_markup())
                        except ValueError:
                            bot.reply_to(message, "Target %s not found." % params[0], reply_markup=targets_markup())
                        bot.register_next_step_handler(msg, process_targets_step)


        if is_admin(message.from_user.id):
            markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
            for ident in identities:
                markup.add(ident.name)
            msg = bot.reply_to(message, "From which identity do you want to remove target?", reply_markup=markup)
            bot.register_next_step_handler(msg, process_identity_step)


    for ident in identities:
        ident.gmail_polling()
    bot.polling(none_stop=True)

if __name__ == '__main__':
    main()
